// Completable in Stable

// Tools
// * Generics
// https://doc.rust-lang.org/book/ch10-00-generics.html 


// Criteria to Complete
// All asserts pass, do not edit function signatures.


/// Fold Data
/// # Arguments
/// `data` Slice of data, of type T
/// `indexes` Indexes within the data to use in the function
/// `fold` A function, used multiple times, 
/// `initialise` - A value of type T 
/// # Example
/// fold_data(&[1_usize, 2_usize, 3_usize], &[0_usize, 1_usize], |x, y| {x + y}, 0_usize)
/// > 3
pub fn fold_data<T : 'static, A>(
    data: &[T],
    indexes : &[usize],
    fold : A,
    initialise : T,
) -> T
    where A : Fn(T, T) -> T,
          T : std::clone::Clone
{
    let mut state = initialise.clone();
    for idx in indexes {
        state = fold(
            state, data[*idx].clone()
        );
    }
    state
}

fn main() {
    let input_a : Vec<f32> = vec![1.0, 24.0, 33.0]; 
    let input_b : Vec<f64> = vec![30.0, 75.0, -5.0];
    let input_c : Vec<usize> = vec![3, 50, 2];

    let indexes : Vec<usize> = vec![0,1,2];
    
    assert_eq!(my_sum(input_a), 58.0);
    
    assert_eq!(my_generic_sum(input_a, &indexes), 58.0);
    assert!(my_generic_mean(input_a, &indexes) > 19.3 && my_generic_mean(input_a, &indexes) < 19.4);
    assert_eq!(my_generic_min(input_a, &indexes), 1.0);
    assert_eq!(my_generic_max(input_a, &indexes), 33.0);

    assert_eq!(my_generic_sum(input_b, &indexes), 100.0);
    assert!(my_generic_mean(input_b, &indexes) > 33.3 && my_generic_mean(input_b, &indexes) < 34.0);
    assert_eq!(my_generic_min(input_b, &indexes), -5.0);
    assert_eq!(my_generic_max(input_b, &indexes), 75.0);

    assert_eq!(my_generic_sum(input_c, &indexes), 55);
    assert_eq!(my_generic_mean(input_c, &indexes), 1);
    assert_eq!(my_generic_min(input_c, &indexes), 0);
    assert_eq!(my_generic_max(input_c, &indexes), 50);
}

fn my_sum(input: &[f32], indexes : &[usize]) -> f32 {    
    fold_data(
        // Add your code here!
    )
}

fn my_generic_sum<T>(input: &[T], indexes : &[usize]) -> T {    
    fold_data(
        // Add your code here!
    )
}

fn my_generic_mean<T>(input: &[T], indexes : &[usize]) -> T {    
    fold_data(
        // Add your code here!
    )
}

fn my_generic_min<T>(input: &[T], indexes : &[usize]) -> T {    
    fold_data(
        // Add your code here!
    )
}

fn my_generic_max<T>(input: &[T], indexes : &[usize]) -> T {    
    fold_data(
        // Add your code here!
    )
}
