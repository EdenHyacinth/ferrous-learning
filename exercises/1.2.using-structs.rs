// Completable in Stable

// Tools
// * Vectors
// https://doc.rust-lang.org/std/vec/struct.Vec.html


// Criteria to Complete
// All asserts pass, do not edit function signatures.

struct DataFrame<T> {
    cols : usize, 
    rows : usize, 
    data : Vec<T>
}

impl<T> DataFrame<T>{
    fn new(cols : usize, rows : usize, data: Vec<T>) -> DataFrame<T>{
        assert!(data.len() == (cols * rows));
        DataFrame {
            cols, 
            rows,
            data
        }
    }
}

fn main() {
    let input_a = DataFrame::new(
    4, 3, 
    vec![
    1,2,3,4,
    5,7,8,9, 
    10,11,12,13
    ]);
    
    let input_b = DataFrame::new(
    2, 4,
    vec![
        0,5,
        10,15,
        20,25,
        30,35
    ]);
    
    assert_eq!(select_column_data(input_a, 1), vec![1,5,10]);
    assert_eq!(select_column_data(input_b, 2), vec![5,15,25,35]);
}

fn select_column_data(input: DataFrame<usize>, column_index : usize) -> Vec<usize> {    
    // Add code here!
}
