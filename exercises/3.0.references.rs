// Completable in Stable

// Tools
// * References
// https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html
// * Generics & Traits
// https://doc.rust-lang.org/book/ch10-00-generics.html 
// * Clone
// https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html#ways-variables-and-data-interact-clone

/*
 * The first function is completing using most of the tools you've used before. 
 * The generic function is more tricky, but more useful. Generic means that it will work for
 * literally /any/ input.
 * And that's very rarely true - it'll work for most inputs, given some reasonable boundaries. To
 * explain to Rust what boundaries we want, we need Trait Parameters. Given the function;
 * fn generic<T> (input: T) where T: std::clone::Clone {input.clone()}
 * You can read it as the following -> "This function can take any input, so long as the input
 * can be cloned (Can fulfil the trait Clone)".
 * For your all_same_generic function, you need to identify what boundaries you want. Rust's
 * compiler is pretty good at telling you what to add, but it might be worth looking at each
 * function on the docs first, and working out what traits you might need.
*/

// It's not required to make a is_all_same_generic for f32s for a really specific reason - look
// into epsilons if you're interested.

// Criteria to Complete
// All asserts pass, do not edit function signatures.

fn main(){
    let input_a : Vec<usize> = vec![0,0,0,0,0];
    let input_b : Vec<usize> = vec![0,0,3,0,0];
    let input_c : Vec<usize> = vec![1,0,0,0,0];
    let input_d : Vec<usize> = vec![0,0,0,0,1];
    
    let input_e : Vec<i32> = vec![0,0,0,0,0];
    let input_f : Vec<usize> = vec![1,1,0,0,0];
    let input_g : Vec<char> = vec!['a','b','c','d','e'];
    let input_h : Vec<char> = vec!['a','a','a','a','a'];

    assert_eq!(is_all_same(&input_a), true);
    assert_eq!(is_all_same(&input_b), false);
    assert_eq!(is_all_same(&input_c), false);
    assert_eq!(is_all_same(&input_d), false);
    
    assert_eq!(is_all_same_generic(input_e), true);
    assert_eq!(is_all_same_generic(input_f), false);
    assert_eq!(is_all_same_generic(input_g), false);
    assert_eq!(is_all_same_generic(input_h), true);
}

fn is_all_same(input: &[usize]) -> bool {
    // Add Code Here!
}

fn is_all_same_generic<T>(input : Vec<T>) -> bool 
    // Add Code Here!
}
