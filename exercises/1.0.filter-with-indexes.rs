// Completable in Stable

// Tools
// * Vectors
// https://doc.rust-lang.org/std/vec/struct.Vec.html
// * Mutability
// https://doc.rust-lang.org/book/ch03-01-variables-and-mutability.html

// Criteria to Complete
// All asserts pass, do not edit function signatures.

fn main() {
    let input_a = vec![2, 2, 2, 2];
    let input_b = vec![2, 1, 2, 7];
    let input_c = vec![1, 3, 5, 2];
    
    assert_eq!(filter_indexes(input_a, vec![1,3]), vec![2,2]);
    assert_eq!(filter_indexes(input_b, vec![0, 1]), vec![2,1]);
    assert_eq!(filter_indexes(input_c, vec![2,3]), vec![5,2]);
}

fn filter_indexes(input: Vec<usize>, indexes : Vec<usize>) -> Vec<usize> {
    // Add Code here!
}
