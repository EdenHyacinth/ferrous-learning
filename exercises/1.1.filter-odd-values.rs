// Completable in Stable

// Tools
// * Options
// https://doc.rust-lang.org/book/ch06-01-defining-an-enum.html#the-option-enum-and-its-advantages-over-null-values
// * Vectors
// https://doc.rust-lang.org/std/vec/struct.Vec.html
// * Iterators
// https://doc.rust-lang.org/book/ch13-02-iterators.html


// Criteria to Complete
// All asserts pass, do not edit function signatures.

fn main() {
    let input_a = vec![2, 2, 2, 2];
    let input_b = vec![2, 1, 2, 7];
    let input_c = vec![1, 3, 5, 2];
    
    assert_eq!(filter_odd_items(input_a), None);
    assert_eq!(filter_odd_items(input_b), Some(vec![1,7]));
    assert_eq!(filter_odd_items(input_c), Some(vec![1,3,5]));
}

fn filter_odd_items(input: Vec<usize>) -> Option<Vec<usize>> {
    // Add Code here!
}

