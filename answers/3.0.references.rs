// Completable in Stable (fun solution in nightly)
// Uncomment following for nightly solution
// #![feature(slice_patterns)]

// Tools
// * References
// https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html


// Criteria to Complete
// All asserts pass, do not edit function signatures.

fn main(){
    let input_a : Vec<usize> = vec![0,0,0,0,0];
    let input_b : Vec<usize> = vec![0,0,3,0,0];
    let input_c : Vec<usize> = vec![1,0,0,0,0];
    let input_d : Vec<usize> = vec![0,0,0,0,1];
    
    let input_e : Vec<i32> = vec![0,0,0,0,0];
    let input_f : Vec<usize> = vec![1,1,0,0,0];
    let input_g : Vec<char> = vec!['a','b','c','d','e'];
    let input_h : Vec<char> = vec!['a','a','a','a','a'];

    assert_eq!(is_all_same(&input_a), true);
    assert_eq!(is_all_same(&input_b), false);
    assert_eq!(is_all_same(&input_c), false);
    assert_eq!(is_all_same(&input_d), false);
    
    assert_eq!(is_all_same_generic(input_e), true);
    assert_eq!(is_all_same_generic(input_f), false);
    assert_eq!(is_all_same_generic(input_g), false);
    assert_eq!(is_all_same_generic(input_h), true);
}

fn is_all_same(input: &[usize]) -> bool {
    let initial_element = &input[0];
    for item in input.iter().skip(1) {
        if item != initial_element {
            return false
        }
    }
    true
} 

fn is_all_same_generic<T>(input : Vec<T>) -> bool 
    where T : std::cmp::PartialEq {
    let initial_element = &input[0];
    for item in input.iter().skip(1) {
        if item != initial_element {
            return false
        }
    }
    true
}
