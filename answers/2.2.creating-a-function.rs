// Completable in Stable

// Tools
// * Functions
// https://doc.rust-lang.org/book/ch03-03-how-functions-work.html


// Criteria to Complete
// All asserts pass, create your own function signatures. Do not edit within main. 

// Notes 
// Please don't use .mean(). I know it's there, and it's easy, 
// but it is just cheating yourself of what we're setting up for. 

fn main() {
    let input_a : Vec<usize> = vec![0,1,2,3,4,5];
    let input_b : Vec<usize> = vec![2,4,8,10,100];

    assert!((my_mean(input_a) - 2.5_f32).abs() < std::f32::EPSILON);
    assert!((my_mean(input_b) - 24.8_f32).abs() < std::f32::EPSILON);
}

fn my_mean(a : Vec<usize>) -> f32 {
    a.iter().map(|x| *x as f32).sum::<f32>() / a.len() as f32
}
