// Completable in Stable 

// Tools
// * Control Flow
// https://doc.rust-lang.org/book/ch03-05-control-flow.html


// Criteria to Complete
// All asserts pass, do not edit function signatures.

pub enum MyType {
    Float(f32),
    Integer(usize),
    BigFloat(f64)
}

fn main(){
    let input_a = MyType::Float(500.0);
    let input_b = MyType::Float(45.0);
    let input_c = MyType::BigFloat(55.0);
    let input_d = MyType::Integer(45);
    let input_e = MyType::Integer(20000000);
    assert_eq!(bigger_than_fifty(input_a), true);
    assert_eq!(bigger_than_fifty(input_b), false);
    assert_eq!(bigger_than_fifty(input_c), true);
    assert_eq!(bigger_than_fifty(input_d), false);
    assert_eq!(bigger_than_fifty(input_e), true);
}

fn bigger_than_fifty(input: MyType) -> bool{
    match input {
        MyType::Float(elem) => elem > 50.0_f32, 
        MyType::Integer(elem) => elem > 50_usize,
        MyType::BigFloat(elem) => elem > 50.0_f64
    }
}
