// Completable in Stable

// Tools
// * Types
// https://doc.rust-lang.org/book/ch03-02-data-types.html

// Criteria to Complete
// All asserts pass, do not edit function signatures.

fn main() {
    let input_a = 20.0_f32;
    let input_b = 0.0_f32;
    assert_eq!(round_to_u32(input_a), 20_u32);
    assert_eq!(round_to_u32(input_b), 0_u32);

    assert_eq!(char_to_digit('2'), 2_u32);
    assert_eq!(char_to_digit('4'), 4_u32);

    assert_eq!(string_to_float("100".to_string()), 100_u32);
    assert_eq!(string_to_float("200".to_string()), 200_u32);

    assert_eq!(is_equal(1.0_f32, 2.0_f32), false);
    assert_eq!(is_equal(1.0_f32, 1.0_f32), true);
}

fn round_to_u32(input: f32) -> u32 {
    input.round() as u32
}

fn char_to_digit(input: char) -> u32 {
    input.to_digit(10).unwrap()
}

fn string_to_float(input: String) -> u32 {
    input.parse::<u32>().unwrap()
}

fn is_equal(lhs: f32, rhs: f32) -> bool {
    (lhs - rhs).abs() <= std::f32::EPSILON
}