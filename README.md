# Ferrous-Learning

Learn Rust through mini-exercises/koans.

Best practice method is to go through https://play.rust-lang.org/ - copy across
the entire exercise you're working on.

When you're happy with your solution, click run. If it works, congrats! Run 'Clippy'
in tools, and Rust will tell you about any improvements it knows about. 

Then, check the answers to see how someone else might've done it, and open a PR if you feel
like your method is more instructive/more rust-y.